# GOFIT-WEB



## Overview
Welcome to [GoFit], your go-to platform for simplifying health management and enhancing your well-being journey. This website is designed with a user-centric approach, offering an integrated experience to effortlessly track, manage, and improve your health and fitness.
Link : https://gofit-xtcb.vercel.app/index.html

## Features

-BMI Calculator:  
Easily assess your health status with our intuitive BMI calculator.

-Exercise Hub:  
Access a curated selection of exercises tailored to various fitness levels and goals.

-Caloric Needs Estimator:  
Determine your daily caloric requirements for a personalized approach to nutrition.

-Access Heathcare:
Quick access to the hospitals nearby.

## Why GoFit?
Unified Experience:

Enjoy a seamless and unified platform that brings together essential health management tools.
User-Friendly Design:

Navigate effortlessly with our intuitive and easy-to-use interface.
Holistic Wellness:

Embrace a holistic approach to health, addressing both physical fitness and overall well-being.


## TechStacks Used
- HTML, CSS and JAVASCRIPT - For Frontend  
- Flex as a layout model in CSS.  
- Framework using bootstrap  
- Open Street Map API


